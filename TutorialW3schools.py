# Python utiliza identación para indicar un bloque de código

if 5 > 2:
    print("¡Cinco es mayor que dos!")

# Python mandará error si se omite la indentación
# if 5 > 2:
# print("¡Cinco es mayor que dos!")

# Los comentarios comienzan con el simbolo #

"""
Existe la posibilidad de generar comentarios 
para múltiples líneas.
x = 10
"""

# Las variables son creadas al momento de asignar un valor a estas
x = 5
y = "¡Hola, Mundo!"

print(x)
print(y)

# Las variables son sensitivas a mayusculas y minusculas

age = 1
Age = 2
AGE = 3

print(age)
print(Age)
print(AGE)

# Asignación de valores a multiples variables en una sola linea

m, c, l , r = "Mario", "Cesar", "Lima", "Rodriguez"
print(m + c + l + r)

# Se puede asignar el mismo valor a multiples variables en una misma linea

a = b = c = "Python"
print(a)
print(b)
print(c)

# Si se intenta combinar variables numericas y cadeba para imprimirlas, se genera error
#print(a + age)

# Para verificar el tipo de cualquier objeto en Python se utiliza la funcion type()

i = 1   # entero
f = 2.9 # flotante
z = 3j  # complejo

print(type(i))
print(type(f))
print(type(z))

# Se puede convertir de un tipo a otro con las funciones int(), float() y complex()

f2 = float(i)   # Entero a flotante
i2 = int(f)     # Flotante a entero
z2 = complex(i) # Entero a complejo

"""
Numeros aleatorios:
Python no tiene una función random() para generar un número aleatorio
Pero tiene una libreria llamada random que puede ser utilizada. 
"""

import random
print(random.randrange(1,1000))

# Se puede asignar una cadena de multiples lineas

s = """Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua."""
print(s)

# Comoo las cadenas son arreglos se puede realizar la subcadena de la siguiente manera:
print(y[2:6])

#La función strip() remueve cualquier espacio en blanco al inicio o al final
s2 = " Saludos! desde Los Cabos...   "
print(s2.strip())

# Para combinar cadenas y numeros se utiliza la funcion format()
cantidad = 15
itemno = 1487
precio = 762.32
orden = "Se requieren {} elementos de tipo {} a precio de {} cada uno."
print(orden.format(cantidad, itemno, precio))

# Se pueden modificar el orden mediante indices especificos
orden = "Se requieren {2} elementos de tipo {1} a precio de {0} cada uno."
print(orden.format(cantidad, itemno, precio))

# Listas
lista = ["Uno", "Dos", "Tres"]
print(lista[1])

lista[1] = "2"

for i in lista:
    print(i)

if "Dos" in lista:
    print("Se encuentra el elemento.")
else:
    print("No se encuentra el elemento.")

if "2" in lista:
    print("Se encuentra el elemento.")
else:
    print("No se encuentra el elemento.")

print("La longitud de la lista es {}.".format(len(lista)))

lista.append("Cuatro")
lista.append("Cinco")
print(lista)

lista.insert(2, "Nuevo")
print(lista)

lista.remove("Nuevo")
print(lista)

lista.pop()
print(lista)

lista.pop(1)
print(lista)

del lista[1]
print(lista)

otralista = lista.copy()
print(otralista)

lista.clear()
print(lista)

lista = list(otralista)
print(lista)

# Tuplas
tupla = ("manzana", "platano", "fresa")
print(tupla)

for x in tupla:
  print(x)

if "fresa" in tupla:
    print("fresa se encuentra en la tupla")
else:
    print("fresa no se encuentra en la tupla")

print("Longitud de la tupla {}".format(len(tupla)))

# Una vez creada la tupla, no se pueden agregar elementos a ella
# tupla[3] = "mango"
# print(tupla)

# No se pueden borrar elementos de la tupla. La tupla es inmodificable.

"""
Una vez borrada la tupla, no se puede utilizar.
del tupla
print(tupla)
"""

# Sets
miset = {"arroz", "frijol", "avena"}
print(miset)

for x in miset:
  print(x)

print("arroz" in miset)

miset.add("trigo")
print(miset)

miset.update(["lenteja", "centeno"])
print(miset)
print(len(miset))

miset.remove("frijol")
print(miset)

miset.discard("centeno")
print(miset)

x = miset.pop()
print(x)
print(miset)

miset.clear()
print(miset)

'''
Una vez borrado el Set, no puede ser utilizado:
del miset
print(miset)
'''

# Diccionarios

"""
Los diccionarios son colecciones que son ordenadas, modificables e indexadas.
Son escritas con { } y tienen claves y valores.
"""

midict = {
    "Codigo": "LI83",
    "Descripcion": "Libreta de notas rustica",
    "Precio": 583
}
print(midict)

# se puede acceder a los elementos del diccionario mediante el nombre de la clave
print(midict["Precio"])

# Alternativamente se puede utilizar el metodo get()
print(midict.get("Codigo"))

# Se puede modificar el valor de un elemento especifico del diccionario mediante su clave
midict["Descripcion"] = "Libreta de notas artesanal"
print(midict)

# Se pueden recorrer los elementos del diccionario mediante el bucle for
for x in midict:
    print(x)

# Para imprimir los valores
for x in midict:
    print(midict[x])

# Se puede utilizar la función values()
for x, y in midict.items():
  print(x, y)

# Se puede agregar nuevos elementos al diccionario
midict["Color"] = "Amarillo"
print(midict)

# El connstructor dict()
thisdict =	dict(brand="Ford", model="Mustang", year=1964)
# note that keywords are not string literals
# note the use of equals rather than colon for the assignment
print(thisdict)

i=0
arregloDicts = []
while i<10:
    arregloDicts.append(dict(brand="Ford" + str(i), model="Mustang"+str(i), year=i+150))
    i+=1

print(arregloDicts)

arregloDicts.clear()
print(arregloDicts)

for x in range(10):
    arregloDicts.append(dict(brand="Ford" + str(x), model="Mustang"+str(x), year=x+150))
print(arregloDicts)

"""
Prueba didactica para generar diccionario de diccionarios
arregloDictsOfDicts = []
for x in range(10):
    arregloDictsOfDicts.append(dict("Dict_"+ str(x), dict(brand="Ford" + str(x), model="Mustang"+str(x), year=x+150)))
print(arregloDictsOfDicts)
"""

# JSON en Python
import json

# Convertir de JSON a Python

x =  '{ "name":"John", "age":30, "city":"New York"}'
y = json.loads(x)
print(type(y))
print(y["age"])

# Covertir de Python a JSON
y = json.dumps(midict)
print(type(y))
print(y)

print(json.dumps(lista))
print(json.dumps(tupla))
# print(json.dumps(miset)) Mandó error (TypeError: Object of type set is not JSON serializable)

x = {
  "name": "John",
  "age": 30,
  "married": True,
  "divorced": False,
  "children": ("Ann","Billy"),
  "pets": None,
  "cars": [
    {"model": "BMW 230", "mpg": 27.5},
    {"model": "Ford Edge", "mpg": 24.1}
  ]
}

print(json.dumps(x, indent=4))

# Manejo de excepciones

try:
  print(mclr)
except:
  print("An exception occurred")

try:
  file = open("demofile.txt")
  file.write("Lorum Ipsum")
except:
  print("Something went wrong when writing to the file")
finally:
  #file.close() Mando error
  print("The 'try except' is finished")
